package com.example.kimamanda.calculadora;

import android.content.pm.InstrumentationInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button limpa, limpaTudo;
    TextView campo_resultado, campo_conta;
    Double primeiroNumero, segundoNumero, resultado;
    String opcaoOperador;
    int auxPonto = 0, auxOperador = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        campo_resultado = (TextView) findViewById(R.id.campo_resultado);
        campo_conta = (TextView) findViewById(R.id.campo_conta);
        limpa = (Button) findViewById(R.id.limpa);
        limpaTudo = (Button) findViewById(R.id.limpaTudo);
    }

    public void mostra0(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "0");
    }

    public void mostra00(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "00");
    }

    public void mostra1(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "1");
    }

    public void mostra2(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "2");
    }

    public void mostra3(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "3");
    }

    public void mostra4(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "4");
    }

    public void mostra5(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "5");
    }

    public void mostra6(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "6");
    }

    public void mostra7(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "7");
    }

    public void mostra8(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "8");
    }

    public void mostra9(View v) {
        campo_resultado.setText(campo_resultado.getText().toString() + "9");
    }

    public void mostraPonto(View v) {

        if (auxPonto == 0) {
            campo_resultado.setText(campo_resultado.getText().toString() + ".");
        }
        auxPonto++;
    }

    public void verifica(View v, String operador) {
        int tamanho = campo_resultado.length();
        auxPonto = 0;

        if (auxOperador == 0) {

            if (tamanho > 0) {
                opcaoOperador = operador;
                primeiroNumero = Double.parseDouble(campo_resultado.getText().toString()); //parseDouble - método para converter uma string em Double
                campo_conta.setText(campo_resultado.getText().toString() + operador);
                campo_resultado.setText("");
            }
        }

        if (auxOperador == 1) {
            campo_resultado.setText(campo_resultado.getText().toString() + "");
        }
    }

    public void opSomar(View v) {
        verifica(v, "+");
    }

    public void opMultiplicar(View v) {
        verifica(v, "x");
    }

    public void opDividir(View v) {
        verifica(v, "÷");
    }

    public void opPorcentagem(View v) {
        verifica(v, "%");
    }

    public void opSubtrair(View v) {
        String auxNegativo = campo_resultado.getText().toString();
        int tamanho = campo_resultado.length();

        if (tamanho == 0) {         //se não tiver número, coloca o sinal negativo
            campo_resultado.setText(campo_resultado.getText().toString() + "-");

        } else {    //se tiver algo escrito

            if ("-".equals(auxNegativo)) {  //se tiver apenas o sinal de negação
                campo_resultado.setText("-");

            } else {
                auxOperador = 1;
                opcaoOperador = "-";
                primeiroNumero = Double.parseDouble(campo_resultado.getText().toString());
                campo_conta.setText(campo_resultado.getText().toString() + "-");
                campo_resultado.setText("");
            }
        }
    }

    public void exibeConta(View v, String operador) {
        campo_resultado.setText("");
        campo_conta.setText(String.valueOf(primeiroNumero) + operador + String.valueOf(segundoNumero));
    }

    public void equacoes(View v) {

        if (opcaoOperador.equals("+")) {
            resultado = primeiroNumero + segundoNumero;
            exibeConta(v, "+");
        }

        if (opcaoOperador.equals("-")) {
            resultado = primeiroNumero - segundoNumero;
            exibeConta(v, "-");
        }

        if (opcaoOperador.equals("x")) {
            resultado = primeiroNumero * segundoNumero;
            exibeConta(v, "x");
        }

        if (opcaoOperador.equals("÷")) {
            resultado = primeiroNumero / segundoNumero;
            exibeConta(v, "÷");
        }

        if (opcaoOperador.equals("%")) {
            resultado = (primeiroNumero * segundoNumero) / 100;
            exibeConta(v, "%");
        }
    }

    public void igual(View v) {

        if (primeiroNumero == null) {
            campo_resultado.setText("");

        } else {
            segundoNumero = Double.parseDouble(campo_resultado.getText().toString());

            if (segundoNumero != null) {
                equacoes(v);
                String stringResultado = Double.toString(resultado);
                String auxResposta = stringResultado.substring(Math.max(stringResultado.length() - 2, 0)); //retorna o maior de um ou mais números.

                if (".0".equals(auxResposta)) {
                    campo_resultado.setText(stringResultado.replace(".0", "").replace("Infinity", "Indefinido").replace("NaN", "Inválido"));

                } else {
                    campo_resultado.setText(String.valueOf(resultado).replace("Infinity", "Indefinido").replace("NaN", "Inválido"));
                }

            } else {
                campo_resultado.setText("");
                campo_conta.setText("");
            }
        }

        auxOperador = 0;
    }


    public void limpar(View v) {
        int tam = campo_resultado.length();

        if (tam > 0) {
            String tamanho = (String) campo_resultado.getText();
            tamanho = tamanho.substring(0, tamanho.length() - 1);
            campo_resultado.setText(tamanho);
            auxPonto = 0;
            auxOperador = 0;
        }

        if (tam < 1) {
            campo_resultado.setText("");
        }
    }

    public void limparTudo(View v) {
        int i = 0;

        for (i = 0; i < 1; i++) {
            campo_conta.setText("");
            campo_resultado.setText("");
        }
        primeiroNumero = null;
        segundoNumero = null;
        auxPonto = 0;
        auxOperador = 0;
    }
}
